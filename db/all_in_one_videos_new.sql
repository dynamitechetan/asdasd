-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2017 at 07:35 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `all_in_one_videos_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `email`, `image`) VALUES
(1, 'admin', 'admin', 'viaviwebtech@gmail.com', 'profile.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `cid` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cid`, `category_name`, `category_image`, `path`) VALUES
(1, 'Animation', '63095_Animation.jpg', ''),
(2, 'Comedy', '91287_Comedy1.jpg', ''),
(3, 'Fashion', '11419_fashion-images.jpg', ''),
(4, 'Music', '10543_music.jpg', ''),
(5, 'Sports', '26013_Sports.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_home_banner`
--

CREATE TABLE `tbl_home_banner` (
  `id` int(11) NOT NULL,
  `banner_name` varchar(255) NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_home_banner`
--

INSERT INTO `tbl_home_banner` (`id`, `banner_name`, `banner_image`, `banner_url`) VALUES
(1, 'Android Drawing', '97286_android_drawing.jpg', 'https://codecanyon.net/item/android-drawing/8193028'),
(2, 'Daily Motion', '9680_Daily_Motion.jpg', 'https://codecanyon.net/item/daily-motion/8239582'),
(3, 'Alphabet', '40905_alphabet.jpg', 'https://codecanyon.net/item/alphabet/8108766');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(11) NOT NULL,
  `app_name` varchar(255) NOT NULL,
  `app_logo` varchar(255) NOT NULL,
  `app_email` varchar(255) NOT NULL,
  `app_version` varchar(255) NOT NULL,
  `app_author` varchar(255) NOT NULL,
  `app_contact` varchar(255) NOT NULL,
  `app_website` varchar(255) NOT NULL,
  `app_description` text NOT NULL,
  `app_developed_by` varchar(255) NOT NULL,
  `app_privacy_policy` text NOT NULL,
  `api_all_order_by` varchar(255) NOT NULL,
  `api_latest_limit` int(3) NOT NULL,
  `api_cat_order_by` varchar(255) NOT NULL,
  `api_cat_post_order_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `app_name`, `app_logo`, `app_email`, `app_version`, `app_author`, `app_contact`, `app_website`, `app_description`, `app_developed_by`, `app_privacy_policy`, `api_all_order_by`, `api_latest_limit`, `api_cat_order_by`, `api_cat_post_order_by`) VALUES
(1, 'All In One Videos App', 'new_logo.png', 'info@viaviweb.in', '1.0.0', 'viaviwebtech', '+91 1234567891', 'www.viaviweb.com', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'Viavi Webtech', '<p><strong>We are committed to protecting your privacy</strong></p>\n\n<p>We collect the minimum amount of information about you that is commensurate with providing you with a satisfactory service. This policy indicates the type of processes that may result in data being collected about you. Your use of this website gives us the right to collect that information.&nbsp;</p>\n\n<p><strong>Information Collected</strong></p>\n\n<p>We may collect any or all of the information that you give us depending on the type of transaction you enter into, including your name, address, telephone number, and email address, together with data about your use of the website. Other information that may be needed from time to time to process a request may also be collected as indicated on the website.</p>\n\n<p><strong>Information Use</strong></p>\n\n<p>We use the information collected primarily to process the task for which you visited the website. Data collected in the UK is held in accordance with the Data Protection Act. All reasonable precautions are taken to prevent unauthorised access to this information. This safeguard may require you to provide additional forms of identity should you wish to obtain information about your account details.</p>\n\n<p><strong>Cookies</strong></p>\n\n<p>Your Internet browser has the in-built facility for storing small files - &quot;cookies&quot; - that hold information which allows a website to recognise your account. Our website takes advantage of this facility to enhance your experience. You have the ability to prevent your computer from accepting cookies but, if you do, certain functionality on the website may be impaired.</p>\n\n<p><strong>Disclosing Information</strong></p>\n\n<p>We do not disclose any personal information obtained about you from this website to third parties unless you permit us to do so by ticking the relevant boxes in registration or competition forms. We may also use the information to keep in contact with you and inform you of developments associated with us. You will be given the opportunity to remove yourself from any mailing list or similar device. If at any time in the future we should wish to disclose information collected on this website to any third party, it would only be with your knowledge and consent.&nbsp;</p>\n\n<p>We may from time to time provide information of a general nature to third parties - for example, the number of individuals visiting our website or completing a registration form, but we will not use any information that could identify those individuals.&nbsp;</p>\n\n<p>In addition Dummy may work with third parties for the purpose of delivering targeted behavioural advertising to the Dummy website. Through the use of cookies, anonymous information about your use of our websites and other websites will be used to provide more relevant adverts about goods and services of interest to you. For more information on online behavioural advertising and about how to turn this feature off, please visit youronlinechoices.com/opt-out.</p>\n\n<p><strong>Changes to this Policy</strong></p>\n\n<p>Any changes to our Privacy Policy will be placed here and will supersede this version of our policy. We will take reasonable steps to draw your attention to any changes in our policy. However, to be on the safe side, we suggest that you read this document each time you use the website to ensure that it still meets with your approval.</p>\n\n<p><strong>Contacting Us</strong></p>\n\n<p>If you have any questions about our Privacy Policy, or if you want to know what information we have collected about you, please email us at hd@dummy.com. You can also correct any factual errors in that information or require us to remove your details form any list under our control.</p>\n', 'ASC', 15, 'category_name', 'DESC');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_version`
--

CREATE TABLE `tbl_version` (
  `vid` int(11) NOT NULL,
  `version_code` varchar(255) NOT NULL,
  `version_messages` varchar(255) NOT NULL,
  `version_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_version`
--

INSERT INTO `tbl_version` (`vid`, `version_code`, `version_messages`, `version_url`) VALUES
(1, '2', 'New Update Available please download it.', 'https://www.google.co.in/');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_video`
--

CREATE TABLE `tbl_video` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `video_type` varchar(255) NOT NULL,
  `video_title` varchar(255) NOT NULL,
  `video_url` text NOT NULL,
  `video_id` varchar(255) NOT NULL,
  `video_thumbnail` text NOT NULL,
  `video_duration` varchar(255) NOT NULL,
  `video_description` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_video`
--

INSERT INTO `tbl_video` (`id`, `cat_id`, `video_type`, `video_title`, `video_url`, `video_id`, `video_thumbnail`, `video_duration`, `video_description`, `status`) VALUES
(1, 2, 'youtube', 'Chup chup ke movie comedy scenes | Rajpal yadav chup chupke', 'https://www.youtube.com/watch?v=xSYjspui1Ps', 'xSYjspui1Ps', '', '07:19', '<p>Chup chup ke movie comedy scenes | Rajpal yadav chup chupkeChup chup ke movie comedy scenes | Rajpal yadav chup chupkeChup chup ke movie comedy scenes | Rajpal yadav chup chupkeChup chup ke movie comedy scenes | Rajpal yadav chup chupke</p>\r\n', 1),
(3, 4, 'dailymotion', 'Zaalima RAEES | HD Video Song', 'http://www.dailymotion.com/video/x57gffr_zaalima-raees-hd-video-song-shahrukh-khan-mahira-khan-arijit-singh-latest-bollywood-songs-2017_music', 'x57gffr', '', '02:52', '<p>Zaalima RAEES | HD Video Song | ShahRukh Khan-Mahira Khan | Arijit Singh | Latest Bollywood Songs 2017</p>\r\n', 1),
(8, 1, 'local', 'Android Kit Kat', 'http://192.168.1.125/all_in_one_videos_new/uploads/70010_kitkat.mp4', '', '35992_android-4.4-kitkat.jpg', '02:52', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 1),
(10, 1, 'vimeo', 'shakira', 'https://vimeo.com/46835688', '46835688', 'http://i.vimeocdn.com/video/326290204_640.jpg', '2:01', '<p>A very hard working day with Alexa.</p>\r\n', 1),
(11, 1, 'vimeo', 'How To Be Confidentt', 'https://vimeo.com/46835688', '46835688', 'http://i.vimeocdn.com/video/326290204_640.jpg', '02:03', '<p>Within this School of Life animation, philosopher and author Alain de Botton imparts some handy wisdom that may help you to become more confident.</p>\r\n', 1),
(12, 2, 'youtube', 'FAST AND FURIOUS 8', 'https://www.youtube.com/watch?v=9PcmKECf82g', '9PcmKECf82g', '', '7:12', '<p>Fast and Furious 8 / The Fate of the Furious Trailer 2017 | Watch the official trailer &amp; clip compilation for &quot;The Fate of the Furious&quot;, an action movie starring Dwayne Johnson, Scott Eastwood &amp; Jason Statham, arriving April 14, 2017 !</p>\r\n', 1),
(13, 4, 'youtube', 'Half Girlfriend Official Trailer', 'https://www.youtube.com/watch?v=KmlBnmyelHI', 'KmlBnmyelHI', '', '02:42', '<p>Balaji Motion Pictures presents &lsquo;Half Girlfriend &ndash; Dost se Zyada, Girlfriend se kam&rsquo;, an adaptation of Chetan Bhagat&#39;s best selling novel &lsquo;Half Girlfriend&rsquo;. Directed by Mohit Suri, the intense love story sets out to explore the &lsquo;grey area&rsquo; in relationships today; the in-between space, between two people.<br />\r\n<br />\r\nStarring Arjun Kapoor, as Madhav Jha &amp; Shraddha Kapoor as Riya Somani, the film is set against the backdrop of three distinct worlds of Delhi, Patna &amp; New York.<br />\r\nMadhav wanted a relationship. Riya didn&#39;t. So, she decided to be his &lsquo;Half Girlfriend&rsquo;.<br />\r\nMore than a friend, les than a girlfriend,</p>\r\n', 1),
(14, 5, 'vimeo', 'BOTHER', 'https://vimeo.com/99751349', '99751349', 'http://i.vimeocdn.com/video/483330670_640.jpg', '02:42', '<p>Video by Harry Israelson &amp; Harry Schleiff</p>\r\n\r\n<p>Music by Chaz Bundick<br />\r\nColor by Mikey Rossiter @ The Mill</p>\r\n\r\n<p>Asst. Producer: Sidney Schleiff<br />\r\nAsst. Producer John Belanger</p>\r\n\r\n<p>Special Thanks:<br />\r\nCooper Rogers<br />\r\nHeath Raymond</p>\r\n', 1),
(15, 4, 'youtube', 'The Flash - First Look', 'https://www.youtube.com/watch?v=mVDGoP4_VYE', 'mVDGoP4_VYE', '', '01:00', '<p>The Fastest Man Alive is coming to TV this fall, Tuesdays on The CW!<br />\r\n<br />\r\nFor more on The Flash:</p>\r\n', 1),
(16, 5, 'youtube', 'Top 5 Luxury Cars 2017', 'https://www.youtube.com/watch?v=yH46mtKn32M', 'yH46mtKn32M', '', '12:25', '<p>Top 5 Luxury Cars 2017</p>\r\n', 1),
(17, 5, 'vimeo', 'Dean Tennant', 'https://vimeo.com/212125324', '212125324', 'http://i.vimeocdn.com/video/628040142_640.jpg', '02:03', '<p>Dean riding on Vancouver Island.</p>\r\n', 1),
(18, 3, 'youtube', 'Jabra Song | FAN', 'https://www.youtube.com/watch?v=d4_szl5EEww', 'd4_szl5EEww', '', '3:12', '<p><br />\r\nA song for the fans by the biggest fan himself. Watch Gaurav go crazy in the&nbsp;&nbsp;&ldquo;Jabra Fan&rdquo;.</p>\r\n', 1),
(19, 3, 'youtube', 'Meri Pyaari Bindu', 'https://www.youtube.com/watch?v=sm_aDtuvfX0', 'sm_aDtuvfX0', '', '2:01', '<p>One of a kind relationship. This time, it&#39;s Bindu vs. Maa! #MeriPyaariBindu Trailer&nbsp;</p>\r\n', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_home_banner`
--
ALTER TABLE `tbl_home_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_version`
--
ALTER TABLE `tbl_version`
  ADD PRIMARY KEY (`vid`);

--
-- Indexes for table `tbl_video`
--
ALTER TABLE `tbl_video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_home_banner`
--
ALTER TABLE `tbl_home_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_version`
--
ALTER TABLE `tbl_version`
  MODIFY `vid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_video`
--
ALTER TABLE `tbl_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
