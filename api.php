<?php include("includes/connection.php");
 	  include("includes/function.php"); 	
	
	$file_path = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/';
 	 
	if(isset($_GET['cat_list']))
 	{
 		$jsonObj= array();
		
		$cat_order=API_CAT_ORDER_BY;


		$query="SELECT cid,category_name,category_image FROM tbl_category ORDER BY tbl_category.".$cat_order."";
		$sql = mysqli_query($mysqli,$query)or die(mysql_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
 
			array_push($jsonObj,$row);
		
		}

		$set['ALL_IN_ONE_VIDEO'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
 	}
	else if(isset($_GET['cat_id']))
	{
		$post_order_by=API_CAT_POST_ORDER_BY;

		$cat_id=$_GET['cat_id'];	

		$jsonObj= array();	
	
	    $query="SELECT * FROM tbl_video
		LEFT JOIN tbl_category ON tbl_video.cat_id= tbl_category.cid 
		where tbl_video.cat_id='".$cat_id."' AND tbl_video.status='1' ORDER BY tbl_video.id ".$post_order_by."";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			$row['video_type'] = $data['video_type'];
			$row['video_title'] = $data['video_title'];
			$row['video_url'] = $data['video_url'];
			$row['video_id'] = $data['video_id'];
			
			if($data['video_type']=='server_url' or $data['video_type']=='local')
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
			else
			{
				$row['video_thumbnail_b'] = $data['video_thumbnail'];
				$row['video_thumbnail_s'] = $data['video_thumbnail'];
			}
 
			$row['video_duration'] = $data['video_duration'];
			$row['video_description'] = $data['video_description'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['ALL_IN_ONE_VIDEO'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

		
	}	 
	else if(isset($_GET['latest']))
	{
		//$limit=$_GET['latest'];	 

		$limit=API_LATEST_LIMIT;

		$jsonObj= array();	
 
		$query="SELECT * FROM tbl_video
		LEFT JOIN tbl_category ON tbl_video.cat_id= tbl_category.cid 
		WHERE tbl_video.status='1' ORDER BY tbl_video.id DESC LIMIT $limit";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			$row['video_type'] = $data['video_type'];
			$row['video_title'] = $data['video_title'];
			$row['video_url'] = $data['video_url'];
			$row['video_id'] = $data['video_id'];

			if($data['video_type']=='server_url' or $data['video_type']=='local')
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
			else
			{
				$row['video_thumbnail_b'] = $data['video_thumbnail'];
				$row['video_thumbnail_s'] = $data['video_thumbnail'];
			}

			$row['video_duration'] = $data['video_duration'];
			$row['video_description'] = $data['video_description'];

 			 

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['ALL_IN_ONE_VIDEO'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}
	else if(isset($_GET['home_banner']))
 	{
 		$jsonObj= array();
		
		 
		$query="SELECT id,banner_name,banner_image,banner_url FROM tbl_home_banner ORDER BY tbl_home_banner.id";
		$sql = mysqli_query($mysqli,$query)or die(mysql_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 

			$row['id'] = $data['id'];
			$row['banner_name'] = $data['banner_name'];
			$row['banner_image'] = $file_path.'images/'.$data['banner_image'];
			$row['banner_image_thumb'] = $file_path.'images/thumbs/'.$data['banner_image'];
			$row['banner_url'] = $data['banner_url'];
 
			array_push($jsonObj,$row);
		
		}

		$set['ALL_IN_ONE_VIDEO'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
 	}
	else if(isset($_GET['home']))
	{
		 
		$jsonObj= array();	
 
		$query="SELECT * FROM tbl_video
		LEFT JOIN tbl_category ON tbl_video.cat_id= tbl_category.cid 
		WHERE tbl_video.status='1' ORDER BY tbl_video.id DESC LIMIT 3";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row1['id'] = $data['id'];
			$row1['cat_id'] = $data['cat_id'];
			$row1['video_type'] = $data['video_type'];
			$row1['video_title'] = $data['video_title'];
			$row1['video_url'] = $data['video_url'];
			$row1['video_id'] = $data['video_id'];
			 
			if($data['video_type']=='server_url' or $data['video_type']=='local')
			{
				$row1['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row1['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
			else
			{
				$row1['video_thumbnail_b'] = $data['video_thumbnail'];
				$row1['video_thumbnail_s'] = $data['video_thumbnail'];
			}

			$row1['video_duration'] = $data['video_duration'];
			$row1['video_description'] = $data['video_description'];

 			 

			$row1['cid'] = $data['cid'];
			$row1['category_name'] = $data['category_name'];
			$row1['category_image'] = $file_path.'images/'.$data['category_image'];
			$row1['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 
			 
			array_push($jsonObj,$row1);
		
		}

		$row['latest_video']=$jsonObj;


		$jsonObj_2= array();	

		$query_all="SELECT * FROM tbl_video
		LEFT JOIN tbl_category ON tbl_video.cat_id= tbl_category.cid 
		WHERE tbl_video.status='1' ORDER BY rand() DESC LIMIT 3";

		$sql_all = mysqli_query($mysqli,$query_all)or die(mysqli_error());

		while($data_all = mysqli_fetch_assoc($sql_all))
		{
			$row2['id'] = $data_all['id'];
			$row2['cat_id'] = $data_all['cat_id'];
			$row2['video_type'] = $data_all['video_type'];
			$row2['video_title'] = $data_all['video_title'];
			$row2['video_url'] = $data_all['video_url'];
			$row2['video_id'] = $data_all['video_id'];
			
			if($data_all['video_type']=='server_url' or $data_all['video_type']=='local')
			{
				$row2['video_thumbnail_b'] = $file_path.'images/'.$data_all['video_thumbnail'];
				$row2['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data_all['video_thumbnail'];
			}
			else
			{
				$row2['video_thumbnail_b'] = $data_all['video_thumbnail'];
				$row2['video_thumbnail_s'] = $data_all['video_thumbnail'];
			}

			$row2['video_duration'] = $data_all['video_duration'];
			$row2['video_description'] = $data_all['video_description'];

 			 

			$row2['cid'] = $data_all['cid'];
			$row2['category_name'] = $data_all['category_name'];
			$row2['category_image'] = $file_path.'images/'.$data_all['category_image'];
			$row2['category_image_thumb'] = $file_path.'images/thumbs/'.$data_all['category_image'];
			
			

			array_push($jsonObj_2,$row2);
		
		}

		$row['all_video']=$jsonObj_2; 

		$set['ALL_IN_ONE_VIDEO'] = $row;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}
	else if(isset($_GET['all_videos']))
	{

		$video_order_by=API_ALL_VIDEO_ORDER_BY;

		$jsonObj= array();	
 
		$query="SELECT * FROM tbl_video
		LEFT JOIN tbl_category ON tbl_video.cat_id= tbl_category.cid 
		WHERE tbl_video.status='1' ORDER BY tbl_video.id $video_order_by";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			$row['video_type'] = $data['video_type'];
			$row['video_title'] = $data['video_title'];
			$row['video_url'] = $data['video_url'];
			$row['video_id'] = $data['video_id'];
			
			if($data['video_type']=='server_url' or $data['video_type']=='local')
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
			else
			{
				$row['video_thumbnail_b'] = $data['video_thumbnail'];
				$row['video_thumbnail_s'] = $data['video_thumbnail'];
			}

			$row['video_duration'] = $data['video_duration'];
			$row['video_description'] = $data['video_description'];

 			 

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['ALL_IN_ONE_VIDEO'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}	
	else if(isset($_GET['video_id']))
	{
		  
				 
		$jsonObj= array();	

		$query="SELECT * FROM tbl_video
		LEFT JOIN tbl_category ON tbl_video.cat_id= tbl_category.cid
		WHERE tbl_video.id='".$_GET['video_id']."'";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 
			
			$row['cat_id'] = $data['cat_id'];
			$row['category_name'] = $data['category_name'];

			$row['id'] = $data['id'];
			$row['video_type'] = $data['video_type'];
			$row['video_title'] = $data['video_title'];
			$row['video_url'] = $data['video_url'];
			$row['video_id'] = $data['video_id'];
			
			if($data['video_type']=='server_url' or $data['video_type']=='local')
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
			else
			{
				$row['video_thumbnail_b'] = $data['video_thumbnail'];
				$row['video_thumbnail_s'] = $data['video_thumbnail'];
			}
			
			$row['video_duration'] = $data['video_duration'];
			$row['video_description'] = $data['video_description'];

			//Related Videos
			$query_2="SELECT * FROM tbl_video
			LEFT JOIN tbl_category ON tbl_video.cat_id= tbl_category.cid
			WHERE tbl_video.cat_id='".$data['cat_id']."' AND tbl_video.status='1' AND tbl_video.id!='".$data['id']."'";

			$sql2 = mysqli_query($mysqli,$query_2)or die(mysqli_error());
 			
 			while($data_2 = mysqli_fetch_assoc($sql2))
			{
				$row2['cat_id'] = $data_2['cat_id'];
				$row2['category_name'] = $data_2['category_name'];

				$row2['id'] = $data_2['id'];
				$row2['video_type'] = $data_2['video_type'];
				$row2['video_title'] = $data_2['video_title'];
				$row2['video_url'] = $data_2['video_url'];
				$row2['video_id'] = $data_2['video_id'];
				
				if($data_2['video_type']=='server_url' or $data_2['video_type']=='local')
				{
					$row2['video_thumbnail_b'] = $file_path.'images/'.$data_2['video_thumbnail'];
					$row2['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data_2['video_thumbnail'];
				}
				else
				{
					$row2['video_thumbnail_b'] = $data_2['video_thumbnail'];
					$row2['video_thumbnail_s'] = $data_2['video_thumbnail'];
				}
				
				$row2['video_duration'] = $data_2['video_duration'];
				$row2['video_description'] = $data_2['video_description'];

				$related_data[]=$row2;

			}
			
			$row['related']=$related_data;


			array_push($jsonObj,$row);
		
		}
 

		$set['ALL_IN_ONE_VIDEO'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();	
 

	}	  	 
	else 
	{
		$jsonObj= array();	

		$query="SELECT * FROM tbl_settings WHERE id='1'";
		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 
			$row['app_name'] = $data['app_name'];
			$row['app_logo'] = $data['app_logo'];
			$row['app_version'] = $data['app_version'];
			$row['app_author'] = $data['app_author'];
			$row['app_contact'] = $data['app_contact'];
			$row['app_email'] = $data['app_email'];
			$row['app_website'] = $data['app_website'];
			$row['app_description'] = $data['app_description'];
 			$row['app_developed_by'] = $data['app_developed_by'];

			$row['app_privacy_policy'] = $data['app_privacy_policy'];
	

			array_push($jsonObj,$row);
		
		}

		$set['ALL_IN_ONE_VIDEO'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();	
	}		
	 
	 
?>